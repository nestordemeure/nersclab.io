# NAMD

[NAMD](https://www.ks.uiuc.edu/Research/namd/) is a molecular dynamics (MD)
program designed for parallel computation.

## Example run scripts

This script requires that `${INPUT_FILE}` be specified. The script is written
such that *only* the number of nodes needs to be changed.

!!! example "Perlmutter GPU"
	```shell
	--8<-- "docs/applications/namd/pm-gpu.sh"
	```

??? example "Perlmutter CPU"
    ```shell
    --8<-- "docs/applications/namd/pm-cpu.sh"
    ```

## Support

 * [User's Guide](https://www.ks.uiuc.edu/Research/namd/current/ug/)
 * [Problem with NAMD?](https://www.ks.uiuc.edu/Research/namd/bugreport.html)

!!! tip
	If *after* the checking the above you believe there is an
	issue with the NERSC module file a ticket with
	our [help desk](https://help.nersc.gov)

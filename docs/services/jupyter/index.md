# Jupyter

Jupyter is a literate computing software ecosystem for working with digital
documents called _notebooks_ that can contain executable code, equations,
visualizations, interactive interfaces, text, and metadata.
Use Jupyter at NERSC to:

* Perform exploratory data analysis and visualization of data stored at NERSC
* Guide machine learning through distributed training, hyperparameter
  optimization, model validation, prediction, and inference
* Manage workflows involving simulation and data analysis on GPU or CPU compute
  nodes
* ... or something else we haven't thought of yet!

To access Jupyter at NERSC, simply visit https://jupyter.nersc.gov with your web
browser, authenticate, and launch a notebook server.

## Jupyter Documentation

The Jupyter documentation is organized into a few sections.

* [How-To](how-to-guides.md): Step-by-step instructions to help you manage your
  Jupyter experience at NERSC.
* [Reference](reference.md): Helpful facts and definitions related to how Jupyter
  is deployed at NERSC.
* [Background](background.md): Context provided to expand user understanding
  about the service.

## Getting Help with Jupyter

If you run into problems with Jupyter at NERSC or just have questions about how
to use it, please use the
[NERSC Help Portal](https://help.nersc.gov) to open a ticket and we will get
back to you as soon as possible.
In that case, before opening your ticket you may want to
[examine your Jupyter logs](reference.md#jupyter-server-logs).

You may also find the #jupyter channel on the
[NERSC User Group Slack](https://www.nersc.gov/users/NUG/nersc-users-slack/)
to be a useful resource where fellow users share knowledge, experiences, and
advice.
But again, to get help from NERSC staff, use the [Help Portal](https://help.nersc.gov).

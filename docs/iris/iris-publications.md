# Tracking publications in Iris

NERSC users can now report and track your publications that used  NERSC resources
in the Iris interface (https://iris.nersc.gov/). There are a number of reasons for you to do this:

Collecting data about your publications is important to NERSC for reporting to DOE
and demonstrating our value and scientific impact so we can continue to provide HPC
resources and services to the scientific community.
Our science communications team looks at publications to find interesting results
to write and publish stories about, put on social media, and make available for NERSC
staff to use as examples when talking about the center to various stakeholders and audiences.
DOE Program Managers look closely at publications while making allocations decisions. This
new publications tracking method will replace the free-form field for listing publications
in future versions of the ERCAP (allocations request and awards) processes. 

## How does it work?

The good news is that if you acknowledged NERSC in your publication, we probably have 
it in our database already. However, even if the publication is in the database, we don’t 
have any way to know it was you among the names in the author list, and we have no way 
to know what project (repo) it was associated with. So we need you to “tag” these publications 
as being associated with you and your project.

If you didn’t acknowledge NERSC in your paper, or the public searches missed your 
publication, you can enter it into Iris by entering its DOI into the system.

We’ve worked hard to make it easy for you to use the publications interface in 
Iris. But it’s a first attempt and feedback is welcome.

## Viewing your tagged publications in Iris

Log into Iris at https://iris.nersc.gov/ and click on the “Profile” tab. Scroll down and
 you’ll see publications associated with your user account. If you see publications that
  are not linked to a project (repo), you can link them to the project using the “Projects”
   button in the “Actions” column.  For your projects, look at the “Details” tab from the
    project overview page.

## Finding and tagging your publications

By tagging or adding a publication, you are identifying it as being associated with you and
 one of your projects. Each publication can be associated with multiple people and projects.

You should search for your “untagged” publications in our database and tag one or more
from the resulting list before attempting to add a new publication to the database.
To do this,  select “Reports->Publications” at the top of Iris, and then click “Run
Report.” This results in a  list of all publications in the Iris database that match
your last name and first initial (in the form Last, FI) in the author list. Use the
“Filter Report” tab to alter this search if desired. Tag publications by clicking 
the box next to your publications and then click “Link Publication(s)” at the top.
 You’ll be asked to add the appropriate project id (repo name, e.g., m342). You can
type in the repo name or leave blank and add it later (by linking again or from your
 user profile page).

## Adding a new publication

If you have a publication that you can’t find in the Iris database, then you need to add
it by going to your “Profile” tab and clicking on the “Add Publication” button and entering
the DOI and project id (repo).

## Quick Summary

View your associated publications
 Iris Profile tab (or Profile from drop down menu at the upper right). Scroll down.

Search and tag publications
Iris Reports Menu -> Publications. Run Report. Tag publications from the list.

Manually add publications not in the Iris database
Iris Profile tab -> Add Publication. Enter DOI and project id (repo).

## Q&A

Q. Someone or project is already associated with my publication, can I associate it with me too?
Yes, publications can be associated with multiple users and projects.

Q. A publication is already associated with my project, but not with me. Should I tag it so
it’s associated with me as well?
Yes. You might want to associate with your account in case NERSC decides to highlight
the research. When we are looking for researchers to comment or give us more details,
we’ll know to contact you. In addition, we are looking at the possibility of Iris being
able to publish citations to your ORCID record. Let us know if you’d be interested in this.
